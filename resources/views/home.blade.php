<!DOCTYPE html>
<html lang="en">
    @include('partials._header')
    <body class="page">
        <div class="site_wrapper">
            @include('partials._navbar')
            @yield('content')
        </div>
        @include('partials._footer')
    </body>
</html>