@extends('admin')
@section('content')
<?php 
    function getRecord($record, $value){
        if($record==null) return old($value);
        return $record->$value;
    }
?>

<section class="content-header">
  <h1>
        Settings
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/badmin') }}"><i class="fa fa-dashboard"></i>Home</a></li>
    <li class="active">Settings</li>
  </ol>
</section>

<section class="content">
    @include('partials._message')
    @include('partials._error')

    <div class="row">
        <form action="{{ url('badmin/spinner') }}" method="post">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Spinner Setting</h3>
                    </div>
                    <div class="box-body">
                        @foreach($data as $row)
                        <div class="form-group">
                            <label for="">{{ $row->spin_type }}</label>
                            <textarea name="spin[{{ $row->id }}]" id="" rows="10" class="form-control">{{ $row->text }}</textarea>
                        </div>
                        @endforeach
                    </div>

                    <div class="box-footer clearfix">
                      <div class="pull-right">
                        <button class="btn bg-olive btn-flat" name="submit" value="1">Save Changes</button>
                      </div>
                    </div
                </div>
            </div>
        </form>
    </div>
</section>


@endsection