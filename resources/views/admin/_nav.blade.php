      <?php 

        if(!isset($navigation)){
          $navigation = [
            'parent' => 'dashboard',
            'child' => null
          ];
        }
      ?>
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ url('/admin_asset/images/mario.png') }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>{{ Auth::user()->name }}</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Site Content</li>
            <li class="{{ $navigation['parent']=='dashboard'?'active':'' }}"><a href="{{ url('/badmin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="{{ $navigation['parent']=='pages'?'active':'' }}"><a href="{{ url('/badmin/pages') }}"><i class="fa fa-file"></i> Pages</a></li>
            <li class="header">Settings</li>
            <li class="{{ $navigation['parent']=='spinner'?'active':'' }}"><a href="{{ url('/badmin/spinner') }}"><i class="fa fa-gear"></i> Spinner</a></li>
            <li class="{{ $navigation['parent']=='profile'?'active':'' }}"><a href="{{ url('/badmin/profile') }}"><i class="fa fa-user"></i> User Profile</a></li>

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>