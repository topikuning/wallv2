<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="canonical" href="{{ Request::url() }}">
    <link rel="shortcut icon" href="{{ url('/canvas_theme/img/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple_icons_57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('/canvas_theme/img/apple_icons_72x72.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('/canvas_theme/img/apple_icons_114x114.png') }}">
    <link rel="alternate" type="application/rss+xml" href="{{ url('rss') }}" title="RSS Feed {{ config('site.site_title') }}">
    @if(!isset($current_title))
        <title>{{ config('site.site_title') }}</title>
    @else
        <title>{{ $current_title }} | {{ config('site.site_title') }}</title>
    @endif
    @if(!isset($no_description))
    @if(!isset($current_description))
    <meta name="description" content="{{ config('site.site_desc') }}">
    @else

    <meta name="description" content="{{ $current_description }}">
    @endif
    @endif

    @if(isset($noindex))
        <meta name="robots" content="noindex,follow" />
    @endif
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ url('/canvas_theme/css/theme.css' )}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{ url('/canvas_theme/css/custom.css') }}" type="text/css" media="all" />
    @include('partials._script')
    <script type="text/javascript">
        var base_url = "{{ url('/') }}";
    </script>
<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":"/page/privacy-policy.html","theme":"dark-bottom"};
</script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<!-- End Cookie Consent plugin -->
</head>
