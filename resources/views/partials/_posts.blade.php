                    @foreach($listing as $row)
                    <?php $ext = pathinfo($row->image_url, PATHINFO_EXTENSION); ?>
                    <div class="isotope-item blogpost_preview_fw">
                        <div class="fw_preview_wrapper">
                            <a href="{{ url($row->code.'-'.$row->slug) }}.html">
                                <div class="pf_output_container">
                                  <?php
                                     $url = url('assets/images/'. $row->slug . '-thumb.' .$ext);
                                     if($row->local_image){
                                        $url = url('/images/thumb/'. $row->thumb_url);
                                     }
                                  ?>
                                  <img src="{{ $url }}" alt="{{ $row->title }}" />
                                </div>
                            </a>
                            <h6 class="blogpost_title"><a href="{{ url($row->slug_keyword) }}">{{ $row->keyword }}</a></h6>
                            <div class="postmeta">
                                <div>
                                  {{ $row->title }}
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach