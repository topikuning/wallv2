    <footer>
        <div class="ip">
            <div class="fl">
                <div><a href="/">{{ config('site.site_footer') }}</a></div>
            </div>
            <div class="fr">
                <div class="socials">
                    &nbsp;
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </footer>  