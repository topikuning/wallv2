@extends('home')
@section('content')
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="main_container">
                        <div class="fl-container span12">
                            <div class="row">
                                <div class="posts-block span12">
                                    <div class="contentarea">
                                        <div class="row">
                                            <div class="span12 module_cont module_blog pb0">
                                                <div class="bloglisting_post row">
                                                    <div class="span12 post_preview">
                                                        <h1 class="entry-title">
                                                            <a href="{{ Request::url() }}">{{ $page->title }}</a>
                                                        </h1>
                                                        <div class="">
                                                            <p>{!! $page->body !!}</p>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                </div>                                             
                                                <div class="horisontal_divider"></div>
   
                                            </div><!-- .module_cont -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        
        </div><!-- .wrapper -->
@endsection