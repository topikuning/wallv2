@extends('home')

@section('content')

    <div class="wrapper">
      <div class="container">
          <div class="filters">
            <h1 class="sub-title">{{ config('site.main_title') }}</h1>
            <p>{{ config('site.main_body') }}</p>
          </div>

          <div class="row">
            <div class="main_container">
              <div class="fl-container span12">
                <div class="fullscreen_block">
                  <h1 class="sub-title">Search  with {{'"'. $search .'"'}}</h1>
                    <div class="fs_blog_module sorting_block" itemscope itemtype="http://schema.org/ImageGallery">
                      <?php $listing = $posts; ?>
                      @include('partials._post_no_key')
                    </div>
                    <ul class="pagerblock">
                      <?php
                        $ispage = $posts->currentPage();
                        $prev = $ispage -1;
                        $next = $ispage + 1;

                      ?>
                      @if($ispage > 1)
                      <li class=""> <a href="{{ url('/search?q='. urlencode($search) .'&page='. $prev) }}">Prev</a></li>
                      @endif

                      @if($posts->hasMorePages())
                      <li class=""><a href="{{ url('/search?q='.urlencode($search) .'&page='. $next) }}">Next</a></li>
                      @endif
                      </ul>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div><!-- .wrapper -->


@endsection
