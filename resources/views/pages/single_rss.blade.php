
<?php $ext = pathinfo($post->image_url, PATHINFO_EXTENSION); ?>
<?php
    $url = url('assets/fullimage/'. $post->slug . '-'. $post->code . '.' .$ext); 
    if($post->local_image){
        $url = url('/images/large/'. $post->image_url);
    } 
?>
<a href="{{ url($post->code .'-'. $post->slug) }}.html" title="{{ $post->title }}">
    <img width="500" src="{{ $url }}" alt="{{ $post->title }}" />
</a>




