@extends('home')
@section('content')

   <?php $ext = pathinfo($post->image_url, PATHINFO_EXTENSION); ?> 

    <?php
        $url = url('assets/fullimage/'. $post->slug . '-'. $post->code . '.' .$ext); 
        if($post->local_image){
            $url = url('/images/large/'. $post->image_url);
        } 
    ?>
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ $current_title }} - {{ config('site.site_title') }}" />
    <meta property="og:url" content="{{ Request::url() }}"/>
    <meta property="og:site_name" content="{{ config('site.site_title') }}" />
    <meta property="og:description" content="{{ ucfirst(strtolower(implode(' ', $body))) }}."/>
    <meta property="og:image" content="{{ $url }}" />
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="main_container">
                        <div class="fl-container span8">
                            <div class="row">
                                <div class="posts-block span12">
                                    <div class="contentarea">
                                        <div class="row">
                                            <div class="span12 module_cont module_blog pb0">
                                                <div class="bloglisting_post row">
                                                    <div class="span12 post_preview">
                                                        
                                                        <div class="pf_output_container">
                                                            <figure class="img-main">
                                                                <img src="{{ $url }}" alt="{{ $post->title }}" />    
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>                                             
                                                
                                            </div><!-- .module_cont -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>   
                        <div class="span4">
                            <div class="sidepanel widget_posts">
                                <h1 class="sub-title">{{ $post->title }}</h1>
                                <p>{{ ucfirst(strtolower(implode(' ', $body))) }}.</p>
                                <div>
                                    <strong>Date </strong> {{ $post->created_at->format('F d,Y') }}
                                </div> 
                                <div>
                                    <strong>Category </strong> <a href="{{ url($post->slug_keyword) }}">{{ $post->keyword }}</a>
                                </div>   
                                <div>
                                    <strong>URL </strong> {{ str_replace(".","[dot]",$post->url) }}
                                </div>   
                                <div>
                                    <strong>
                                        <a href="{{ $url }}" title="Download {{ $post->title }} image" download>DOWNLOAD FULL IMAGE</a>
                                    </strong>
                                </div>
                                <div class="meta">
                                    <span>Share on</span>
                                    <span> <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::url()) }}" title="Facebook"><i class="icon-facebook"></i></a></span>
                                    <span><a target="_blank" href="https://twitter.com/intent/tweet?text={{ urlencode($post->title) }}&url={{ urlencode(Request::url()) }}&related=" title="Twitter"><i class="icon-twitter"></i></a></span>
                                    <span><a target="blank" href="https://plus.google.com/share?url={{ urlencode(Request::url()) }}" title="G+"><i class="icon-google-plus"></i></a></span>
                                </div>                        
                            </div>
                        </div>
                        
                        <div class="clear"></div>
                    </div>
                </div>

                @include('partials._related')

            </div>
        
        </div><!-- .wrapper -->
@endsection
