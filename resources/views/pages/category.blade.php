@extends('home')

@section('content')

    <div class="wrapper">
      <div class="container">
          <div class="filters">
            <h1 class="sub-title">{{ $category }}</h1>
            <p>{{ $body }}..</p>
          </div>
          <div class="row">
            <div class="main_container">
              <div class="fl-container span12">
                <div class="fullscreen_block">
                    <div class="fs_blog_module sorting_block">
                      <?php $listing = $posts; ?>
                      @include('partials._post_no_key')
                    </div>
                </div>
              </div>
            </div>
          </div>
      </div>


    </div><!-- .wrapper -->


@endsection
