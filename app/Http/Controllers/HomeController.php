<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Libs\Superspin\SuperSpin;


class HomeController extends Controller {

    public function index($page=1)
    {
        if(!is_numeric($page)) $page = 1;
        $limit = 12; 
        $start = ($page -1) * $limit;


        $sql = DB::table('post_keyword')
                 ->select('posts.*')
                 ->join('posts', 'post_keyword.last_post', '=', 'posts.id')
                 ->where('post_keyword.created_at', '<=', DB::raw('now()'))
                 ->orderBy('post_keyword.id','DESC');
   
        /**
        $sql = DB::table('posts')
               ->where('created_at','<', DB::raw('now()'))
               ->orderBy('id','DESC');
        **/

        $total = $sql->count(); 
        $data  = $sql->take($limit)->skip($start)->get();

        $posts = [
            'data' => $data,
            'total' => $total,
            'limit' => $limit
        ];


        $result= ['posts' => $posts];

        if($page > 1){
            $result['current_title'] = 'Page '. $page; 
            $result['noindex'] = true;
        }

        return view('pages.main', $result);

    }


    public function searchData(Request $request)
    {
        $q = $request->input('q'); 
        $q = trim($q); 
        if(!$q) return $this->index(); 

        $posts = DB::table('posts')
                    ->whereRaw("MATCH(title,keyword) AGAINST(? IN BOOLEAN MODE)", array($q))
					->orderBy('title') 
                    ->paginate(12);

        $page = ' page '. $posts->currentPage();


        return view('pages.search',[
                'posts' => $posts,
                'search' => $q,
                'current_title' => 'Search ' . $q . $page,
                'no_description' => true
            ]);



    }


    public function getSingle($slug)
    {
        $code = explode('-', $slug);
        $post = DB::table('posts')
                ->where('created_at', '<=', DB::raw('now()'))
                ->where('code', $code[0])
                ->first();
        if(!$post) return $this->index();
        $post->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at);
        //if slow dont update the db 
        $statement = 'UPDATE posts SET viewed = viewed +1 WHERE id = '. $post->id . ' LIMIT 1'; 
        DB::statement($statement);

        $post_random = explode(',', $post->post_random);

       $details = [];
       if($post->post_random){
         $query = 'SElECT * FROM posts WHERE id != '. $post->id . ' AND id IN('.$post->post_random.') ORDER BY FIELD(id, '.$post->post_random.')';
         $details = DB::select($query);
       }
        $body = [];
        $desc = [];

        foreach ($details as $key => $value) {
            if($key < 4){
                $body[] = $value->title;
            } 
            if($key < 3){
                $desc = $body;
            }       
        }

        $now = Carbon::now();
        //RELATED
		$related = DB::table('posts')
                    ->whereRaw("MATCH(title,keyword) AGAINST(? IN BOOLEAN MODE)", array($post->title))
                    ->paginate(8); 
		
		if(!count($related)){
            $related = $details;
        }
        $tmp = []; 
        foreach($related as $row){
            $created_at = Carbon::createFromFormat('Y-m-d H:i:s',$row->created_at); 
            if($created_at->lte($now)){
                $tmp[] = $row;
            }
        }

 
        return view('pages.single',
            ['post' => $post,
              'body' => $body,
             'details' => $tmp,
             'current_title' => $post->title .', '. $post->keyword,
             'current_description' => implode('. ', $desc) .'. '. config('site.site_title')  
            ]);
    }


    public function getPage($slug)
    {
        $data = DB::table('pages')
                    ->where('published','1')
                    ->where('slug', $slug)
                    ->first(); 

        if(!$data) return $this->index();



        return view('pages.page',
            ['page' => $data,
             'current_title' =>$data->title,
             'noindex' => true,
             'current_description' => str_limit($data->body,50)
            ]);
        
    }
}