<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use finfo;
use App\Models\timthumb;

class ImageController extends Controller {


    public function getFullImage($slug, Request $request)
    {
 

        $ori_slug = $slug;
        $arr = explode('.', $slug); 
        $slug = $arr[0];
        $arr_slug = explode('-', $slug); 
        array_pop($arr_slug); 

        $slug = implode('-', $arr_slug);
        $data = DB::table('posts')->where('slug', $slug)->first(); 
         
        if(!$data) abort(404); 
        $url =  $data->image_url;
        //timthumb::start($url,0,0);


        
       ignore_user_abort(true);
       set_time_limit(0);

        $tmpfile = base_path() .'/storage/app/'.str_random(8) .'.tmp';
        $fp = fopen ($tmpfile, 'w+');//This is the file where we save the    information
        $ch = curl_init($url);//Here is the file we are downloading, replace spaces with %20
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.122 Safari/534.30');
        curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch); // get curl response
        curl_close($ch);
        fclose($fp);

        $image_mime = image_type_to_mime_type(exif_imagetype($tmpfile));
        $created = Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at);
        $last_modified = $created->format('D, d M Y H:i:s \G\M\T');
        $expires = $created->addMonth()->format('D, d M Y H:i:s \G\M\T');
        $size = filesize($tmpfile);
        header('Content-Type: '.$image_mime); 
        //header("Content-type: application/octet-stream");
        header('Content-length: '. $size);
        header('Last-Modified: '. $last_modified); 
        header('Expires: '. $expires); 
        header('Cache-Control: public');
        $this->readfile_chunked($tmpfile);
        unlink($tmpfile);



    }


    public function readfile_chunked($filename, $retbytes = TRUE) 
      {
        $buffer = "";
        $cnt =0;
        $handle = fopen($filename, "rb");
        if ($handle === false) {
          return false;
        }
        while (!feof($handle)) {
          $buffer = fread($handle, 1024*1024);
          echo $buffer;
          ob_flush();
          flush();
          if ($retbytes) {
            $cnt += strlen($buffer);
          }
        }
        $status = fclose($handle);
        if ($retbytes && $status) {
          return $cnt; // return num. bytes delivered like readfile() does.
        }
        return $status;
      }

    public function getImage($slug, Request $request)
    {
        $ori_slug = $slug;
        $arr = explode('.', $slug); 
        $slug = $arr[0];
        $arr_slug = explode('-', $slug); 
        array_pop($arr_slug); 
        $slug = implode('-', $arr_slug);
        $data = DB::table('posts')->where('slug', $slug)->first(); 
         
        if(!$data) abort(404); 
        //$url =  $data->thumb_url;
        //timthumb::start($url,250);
        $filename =  $data->thumb_url;

        $created = Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at);
        $last_modified = $created->format('D, d M Y H:i:s \G\M\T');
        $expires = $created->addMonth()->format('D, d M Y H:i:s \G\M\T');

 
        $content = $this->getContent($filename);
        if(!$content) return redirect(url('img/placeholder.png'));


        $image_mime = $this->getMime($content);
        

        return response($content)->header('Content-Type', $image_mime)
                                         ->header('Last-Modified',$last_modified)
                                         ->header('Expires',$expires)
                                        ->header('Cache-Control','public');


    }

   private function getContent($filename)
    {
        if(true === function_exists('curl_init')){
              $ch = curl_init($filename);
              curl_setopt_array(
                $ch,
                array(
                   CURLOPT_SSL_VERIFYPEER  => false,
                   CURLOPT_RETURNTRANSFER  => true,
                   CURLOPT_USERAGENT       => "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.122 Safari/534.30"
                )
              );
              $content = curl_exec($ch);
              curl_close($ch);
        }else{
            $context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
            $content = file_get_contents($filename, false, $context);
        }

        return $content; 
    }

    private function getMime($content)
    {


        //with fileinfo
        $file_info = new finfo(FILEINFO_MIME);  // object oriented approach!
        $mime_type = $file_info->buffer($content);
        $mime = explode(';', $mime_type); 
        $image_mime = $mime[0];
        return $image_mime;

    }



}