<?php namespace App\Composers;

use DB;
use stdClass;

class ComposerNavigation {

    public function setSidebar($view)
    {
        $categories = DB::table('categories')
                        ->where('published','1')
                        ->orderBy('title')
                        ->get();
        
        $best_rated = DB::table('post_keyword')
                        ->orderBy('ratingValue','DESC')
                        ->take(6)
                        ->get();
        $last_viewed =  DB::table('post_keyword')
                        ->orderBy('last_viewed','DESC')
                        ->take(5)
                        ->get();

        $data = [
            'categories' => $categories,
            'best_rated' => $best_rated,
            'last_viewed' => $last_viewed
        ];
        $view->with($data);
    }

    public function getPage($view)
    {
        $pages = DB::table('pages')
                     ->where('published','1')
                     ->orderBy('title')
                     ->get();
        $view->with(['pages' => $pages]);
    }



}

