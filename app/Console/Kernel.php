<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //'App\Console\Commands\UpdateSiteMap',
        'App\Console\Commands\ImportKeywords',
        'App\Console\Commands\GeneratePost',
        'App\Console\Commands\ResetDatabase',
        'App\Console\Commands\PostTweet',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('content:scrap')->dailyAt(config('site.run_scrap_at'));
        if(config('site.auto_post_twit')){
            //run every two hours
            $schedule->command('tweet:post')->cron('0 */2 * * *');
        }
    }
}
