<?php 

return [
    'site_url' => 'http://localhost.id:8000',
    'site_title' => 'Sitename Free Wallpaper',
    'site_desc' => 'Free wallpaper',
    'site_footer' => 'Copyright 2015',
    'main_title' => 'Hi, Welcome to sitename',
    'main_body' => 'Coolest Gallery of Designs Home Interior',
    'image_cache' => false,
    'fb_link' => '#',
    'tw_link' => '#',
    'gplus_link' => '#',
    'node_server_url' => 'http://localhost:4000',
    'g_suggest' => true,
    'keywords_per_scrap' => '10',
    'engine_scrap' => 'bing', //bing or yandex
    'run_scrap_at' => '23:00',
    'auto_post_twit' => false, 
    'has_tag'  => ['#home','#kitchen','#furniture','#design','#interiordesign','#decor'],
    'add_word' => ['Cool ', 'Awesome ', 'Top ', 'Best '],
    'local_image' => false,
    'thintumb_url' => 'http://localhost.id/resimg/index.php',
];
