<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastViewedToPostKeyword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_keyword', function (Blueprint $table) {
            $table->timestamp('last_viewed')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_keyword', function (Blueprint $table) {
            $table->dropColumn('last_viewed');
        });
    }
}
